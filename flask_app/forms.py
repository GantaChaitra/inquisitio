import wand
from wand.display import display
from wand.image import Image as imgw
from PIL import Image
import os
import numpy as np
import cv2
from flask import Flask, request, render_template
from werkzeug.utils import secure_filename
from tensorflow.python.platform import gfile
import tensorflow.compat.v1 as tf
import re
app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.jpeg','.png','.jpg']
global n
n = 0
font = cv2.FONT_HERSHEY_SIMPLEX
id = 0
names = ['None', 'shivani', 'Paula', 'Ilza', 'Z', 'W']
#img =  cv2.imread('/home/shivani/Desktop/image/pics/profile.png')
tf.disable_eager_execution()
Before = ""
After = ""

def prewhiten(x):
    mean = np.mean(x)
    std = np.std(x)
    std_adj = np.maximum(std, 1.0 / np.sqrt(x.size))
    y = np.multiply(np.subtract(x, mean), 1 / std_adj)
    return y


def to_rgb(img):
    w, h = img.shape
    ret = np.empty((w, h, 3), dtype=np.uint8)
    ret[:, :, 0] = ret[:, :, 1] = ret[:, :, 2] = img
    return ret


def get_model_filenames(model_dir):
    files = os.listdir(model_dir)
    meta_files = [s for s in files if s.endswith('.meta')]
    if len(meta_files) == 0:
        raise ValueError('No meta file found in the model directory (%s)' % model_dir)
    elif len(meta_files) > 1:
        raise ValueError('There should not be more than one meta file in the model directory (%s)' % model_dir)
    meta_file = meta_files[0]
    meta_files = [s for s in files if '.ckpt' in s]
    max_step = -1
    for f in files:
        step_str = re.match(r'(^model-[\w\- ]+.ckpt-(\d+))', f)
        if step_str is not None and len(step_str.groups()) >= 2:
            step = int(step_str.groups()[1])
            if step > max_step:
                max_step = step
                ckpt_file = step_str.groups()[0]
    return meta_file, ckpt_file


def make_image_tensor(img, image_size, do_prewhiten=True):
    image = np.zeros((1, image_size, image_size, 3))
    if img.ndim == 2:
        img = to_rgb(img)
    if do_prewhiten:
        img = prewhiten(img)
    image[0, :, :, :] = img
    return image


def make_images_tensor(img1, img2, image_size, do_prewhiten=True):
    images = np.zeros((2, image_size, image_size, 3))
    for i, img in enumerate([img1, img2]):
        if img.ndim == 2:
            img = to_rgb(img)
        if do_prewhiten:
            img = prewhiten(img)
        images[i, :, :, :] = img
    return images


def load_model(model, session):
    model_exp = os.path.expanduser(model)
    if os.path.isfile(model_exp):
        with gfile.FastGFile(model_exp, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            tf.import_graph_def(graph_def, name='')
    else:
        meta_file, ckpt_file = get_model_filenames(model_exp)

        saver = tf.train.import_meta_graph(os.path.join(model_exp, meta_file))
        saver.restore(session, os.path.join(model_exp, ckpt_file))


class Verification:
    def __init__(self):
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)
        self.session = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
        self.images_placeholder = ''
        self.embeddings = ''
        self.phase_train_placeholder = ''
        self.embedding_size = ''
        self.session_closed = False

    def __del__(self):
        if not self.session_closed:
            self.session.close()

    def kill_session(self):
        self.session_closed = True
        self.session.close()

    def load_model(self, model):
        load_model(model, self.session)

    def initial_input_output_tensors(self):
        self.images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        self.embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        self.phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
        self.embedding_size = self.embeddings.get_shape()[1]

    def img_to_encoding(self, img, image_size):
        image = make_image_tensor(img, image_size)

        feed_dict = {self.images_placeholder: image, self.phase_train_placeholder: False}
        emb_array = np.zeros((1, self.embedding_size))
        emb_array[0, :] = self.session.run(self.embeddings, feed_dict=feed_dict)

        return np.squeeze(emb_array)


class FaceDetection:

    # Modify the verification_threshold incase you want to edit 
    verification_threshold = 0.8
    v, net = None, None

    def __init__(self):
        FaceDetection.net = FaceDetection.load_opencv()
        FaceDetection.v = FaceDetection.load_model()

    @staticmethod
    def load_opencv():
        model_path = "./Models/OpenCV/opencv_face_detector_uint8.pb"
        model_weights = "./Models/OpenCV/opencv_face_detector.pbtxt"
        net = cv2.dnn.readNetFromTensorflow(model_path, model_weights)
        return net

    @staticmethod
    def load_model():
        v = Verification()
        v.load_model("./Models/FaceDetection/")
        v.initial_input_output_tensors()
        return v

    @staticmethod
    def is_same(emb1, emb2):
        diff = np.subtract(emb1, emb2)
        diff = np.sum(np.square(diff))
        return diff < FaceDetection.verification_threshold, diff

    @staticmethod
    def fetch_embeddings(image):

        image_size = 160
        height, width, channels = image.shape

        blob = cv2.dnn.blobFromImage(image, 1.0, (300, 300), [104, 117, 123], False, False)
        FaceDetection.net.setInput(blob)
        detections = FaceDetection.net.forward()

        faces = []

        for i in range(detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > 0.5:
                x1 = int(detections[0, 0, i, 3] * width)
                y1 = int(detections[0, 0, i, 4] * height)
                x2 = int(detections[0, 0, i, 5] * width)
                y2 = int(detections[0, 0, i, 6] * height)
                faces.append([x1, y1, x2 - x1, y2 - y1])

                # cv2.rectangle(image, (x1, y1), (x2, y2), (255, 0, 0), 2)
                # cv2.imshow("img", image)
                # cv2.waitKey(0)

        if len(faces) == 1:
            face = faces[0]
            x, y, w, h = face
            im_face = image[y:y + h, x:x + w]
            img = cv2.resize(im_face, (200, 200))
            user_embed = FaceDetection.v.img_to_encoding(cv2.resize(img, (160, 160)), image_size)
        else:
            return None

        return user_embed

    @staticmethod
    def verify_face(image1, image2):

        if not FaceDetection.v:
            FaceDetection.v = FaceDetection.load_model()

        if not FaceDetection.net:
            FaceDetection.net = FaceDetection.load_opencv()

        img1_emb = FaceDetection.fetch_embeddings(image1)
        img2_emb = FaceDetection.fetch_embeddings(image2)

        if img1_emb is not None and img2_emb is not None:
            response = FaceDetection.is_same(img1_emb, img2_emb)
            return {"response": "API result", "verified": str(response[0]), "accuracy": response[1]}

        cv2.destroyAllWindows()
        return {"response": "Face unavailable in either image", "verified": str(False), "accuracy": 0}
##############################################


class FaceDetection:
    verification_threshold = 0.8
    v, net = None, None
    image_size = 160

    def __init__(self):
        FaceDetection.load_models()

    @staticmethod
    def load_models():
        if not FaceDetection.net:
            FaceDetection.net = FaceDetection.load_opencv()

        if not FaceDetection.v:
            FaceDetection.v = FaceDetection.load_face_detection()
        
    @staticmethod
    def load_opencv():
        model_path = "./Models/OpenCV/opencv_face_detector_uint8.pb"
        model_pbtxt = "./Models/OpenCV/opencv_face_detector.pbtxt"
        net = cv2.dnn.readNetFromTensorflow(model_path, model_pbtxt)
        return net

    @staticmethod
    def load_face_detection():
        v = Verification()
        v.load_model("./Models/FaceDetection/")
        v.initial_input_output_tensors()
        return v

    @staticmethod
    def is_same(emb1, emb2):
        diff = np.subtract(emb1, emb2)
        diff = np.sum(np.square(diff))
        return diff < FaceDetection.verification_threshold, diff

    @staticmethod
    def detect_faces(image, display_images=False): # Make display_image to True to manually debug if you run into errors
        height, width, channels = image.shape

        blob = cv2.dnn.blobFromImage(image, 1.0, (300, 300), [104, 117, 123], False, False)
        FaceDetection.net.setInput(blob)
        detections = FaceDetection.net.forward()

        faces = []

        for i in range(detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > 0.5:
                x1 = int(detections[0, 0, i, 3] * width)
                y1 = int(detections[0, 0, i, 4] * height)
                x2 = int(detections[0, 0, i, 5] * width)
                y2 = int(detections[0, 0, i, 6] * height)
                faces.append([x1, y1, x2 - x1, y2 - y1])

                if display_images:
                    cv2.rectangle(image, (x1, y1), (x2, y2), (255, 0, 0), 3)
    
        if display_images:
            print("Face co-ordinates: ", faces)
            cv2.imshow("Training Face", cv2.resize(image, (300, 300)))
            cv2.waitKey(0)
        return faces

    @staticmethod
    def load_face_embeddings(image_dir):

        embeddings = {}
        for file in os.listdir(image_dir):
            img_path = image_dir + file
            try:
                image = cv2.imread(img_path)
                faces = FaceDetection.detect_faces(image)
                if len(faces) == 1:
                    x, y, w, h = faces[0]
                    image = image[y:y + h, x:x + w]
                    embeddings[file.split(".")[0]] = FaceDetection.v.img_to_encoding(cv2.resize(image, (160, 160)), FaceDetection.image_size)
                else:
                    print(f"Found more than 1 face in \"{file}\", skipping embeddings for the image.")
            except Exception:
                print(f"Unable to read file: {file}")
        return embeddings

    @staticmethod
    def fetch_detections(image, embeddings, display_image_with_detections=False):
        
        faces = FaceDetection.detect_faces(image)
        
        detections = []
        for face in faces:
            x, y, w, h = face
            im_face = image[y:y + h, x:x + w]
            img = cv2.resize(im_face, (200, 200))
            user_embed = FaceDetection.v.img_to_encoding(cv2.resize(img, (160, 160)), FaceDetection.image_size)
            
            detected = {}
            for _user in embeddings:
                flag, thresh = FaceDetection.is_same(embeddings[_user], user_embed)
                if flag:
                    detected[_user] = thresh
            
            detected = {k: v for k, v in sorted(detected.items(), key=lambda item: item[1])}
            detected = list(detected.keys())
            if len(detected) > 0:
                detections.append(detected[0])

                #if display_image_with_detections:
                cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
                cv2.putText(image, detected[0], (x, y - 4), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        

        #if display_image_with_detections:
        #cv2.imshow("Detected", cv2.resize(image, (300, 300)))
        filename = '/home/shivani/Desktop/flask_app/static/DetectedImage.jpg'
        cv2.imwrite(filename,cv2.resize(image, (300, 300)))

        return detections


def face_recognition(image_or_video_path=None, display_image=False, face_dir="faces/"):
    FaceDetection.load_models()
    embeddings = FaceDetection.load_face_embeddings(face_dir)
    waitkey_variable = 1
    image_flip = False

    if image_or_video_path:
        print("Using path: ", image_or_video_path)
        cap = cv2.VideoCapture(image_or_video_path)
        if int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) == 1:
            waitkey_variable = 0
    else:
        print("Capturing from webcam")
        image_flip = True
        cap = cv2.VideoCapture(0)
    
    while 1:
        ret, image = cap.read()
        if image_flip:
	        image = cv2.flip(image, 1)

        if not ret:
            print("Finished detection")
            return

        print(FaceDetection.fetch_detections(image, embeddings, display_image))

        key = cv2.waitKey(waitkey_variable)
        if key & 0xFF == ord("q"):
            break

    cap.release()
    cv2.destroyAllWindows()
    
@app.route('/', methods=["GET", "POST"])
def deblur():
    data = []
    if request.method == "POST":
        file = request.files["file"]
        file_name = os.path.join(
            '/home/shivani/Desktop/flask_app/static/', secure_filename(file.filename))
        file.save(file_name)
        print(secure_filename(file.filename))
        Before = secure_filename(file.filename)
        with imgw(filename=file_name) as img:
            #img1 = img
            img.adaptive_sharpen(radius=10, sigma=12)
            img.enhance()
            img.unsharp_mask(radius=10, sigma=12, amount=2, threshold=0)
            #img1.save(filename = file_name)
            img.save(
                filename="/home/shivani/Desktop/flask_app/static/suspect.jpeg")
            After = "suspect.jpeg"
            data.append(Before)
            data.append(After)
            data.append('1')
            print(data)
           # n = n + 1
    return render_template("forms.html",data=data)

@app.route('/colorize', methods=["GET", "POST"])
def colorize():
    data = []
    if request.method == "POST":
        net = cv2.dnn.readNetFromCaffe('./model/colorization_deploy_v2.prototxt', './model/colorization_release_v2.caffemodel')
        pts = np.load('./model/pts_in_hull.npy')
        class8 = net.getLayerId("class8_ab")
        conv8 = net.getLayerId("conv8_313_rh")
        pts = pts.transpose().reshape(2, 313, 1, 1)
        net.getLayer(class8).blobs = [pts.astype("float32")]
        net.getLayer(conv8).blobs = [np.full([1, 313], 2.606, dtype='float32')]
        file = request.files["file"]
        file_name = os.path.join('/home/shivani/Desktop/flask_app/static/', secure_filename(file.filename))
        file.save(file_name)
       # Before = secure_filename(file.filename)
        image = cv2.imread(file_name)
        scaled = image.astype("float32")/255.0
        lab = cv2.cvtColor(scaled, cv2.COLOR_BGR2LAB)
        resized = cv2.resize(lab, (224, 224))
        L = cv2.split(resized)[0]
        L -= 50
        net.setInput(cv2.dnn.blobFromImage(L))  
        ab = net.forward()[0, :, :, :].transpose((1, 2, 0))
        ab = cv2.resize(ab, (image.shape[1], image.shape[0]))
        L = cv2.split(lab)[0]
        colorized = np.concatenate((L[:, :, np.newaxis], ab), axis=2)
        colorized = cv2.cvtColor(colorized, cv2.COLOR_LAB2BGR)
        colorized = np.clip(colorized, 0, 1)
        colorized = (255 * colorized).astype("uint8")
        #cv2.imshow("Original", image)
        #cv2.imshow("Colorized", colorized)
        #img = Image.fromarray(colorized, 'RGB')       
        cv2.imwrite('/home/shivani/Desktop/flask_app/static/color.png',colorized)
       # After = "color.png"
        data.append(secure_filename(file.filename))
        data.append("color.png")
        data.append('2')
        #data = [secure_filename(file.filename),"color.png"]
        #colorized.save(filename="/home/shivani/Desktop/flask_app/uploads/color.png")        
       # cv2.waitKey(0)
    return render_template("forms.html",data=data)

@app.route('/recogntion', methods=["GET", "POST"])
def recognition():
    data = []
    file = request.files["file"]
    file_name = os.path.join('/home/shivani/Desktop/flask_app/static/', secure_filename(file.filename))
    file.save(file_name)
    data.append(secure_filename(file.filename))
    input_path = file_name
    display_image = "" 
    faces_dir = "faces/"
    img1 = cv2.imread(input_path)
    laplacian_var = cv2.Laplacian(img1, cv2.CV_64F).var()
    print(laplacian_var)
    if laplacian_var < 10:
        with imgw(filename=file_name) as img:
            img.adaptive_sharpen(radius=10, sigma=12)
            img.enhance()
            img.unsharp_mask(radius=10, sigma=12, amount=2, threshold=0)
            img.save(
                filename="/home/shivani/Desktop/flask_app/static/df.jpeg")
            input_path = "/home/shivani/Desktop/flask_app/static/df.jpeg"
    face_recognition(input_path,display_image,faces_dir)
    data.append("DetectedImage.jpg")
    data.append('3')
    return render_template("forms.html",data=data)


if __name__ == '__main__':
    app.run()
